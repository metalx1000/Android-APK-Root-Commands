package com.filmsbykris.sysrootcmd;

import java.util.*;
import java.net.*;
import java.io.*;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends Activity {

  TextView tv;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    tv=(TextView)findViewById(R.id.cmdOp);
    Thread t=new Thread(){
      int count = 0;

      @Override
      public void run(){

        while(!isInterrupted()){

          try {

            runOnUiThread(new Runnable() {

              @Override
              public void run() {
                count++;
                tv.setText("Count: ");
                tv.append(String.valueOf(count));
                tv.append("\n");
                tv.append(shell_root("whoami"));
                tv.append(shell_root("date"));

              }
            });

            Thread.sleep(1000);  //1000ms = 1 sec
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        }
      }
    };

    t.start();
  }


  public String shell_root(String cmd){
    String o="";
    try {
      Process process = Runtime.getRuntime().exec("su");
      OutputStream stdin = process.getOutputStream();
      InputStream stderr = process.getErrorStream();
      InputStream stdout = process.getInputStream();

      //stdin.write(("ls\n").getBytes());
      stdin.write((cmd+"\n").getBytes());
      stdin.write("exit\n".getBytes());
      stdin.flush();

      stdin.close();
      BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
      String line;
      while ((line = br.readLine()) != null) {
        o+=line + "\n";
        Log.d("[Output]", line);
      }
      br.close();
      br =
        new BufferedReader(new InputStreamReader(stderr));
      while ((line = br.readLine()) != null) {
        o+=line + "\n";
        Log.e("[Error]", line);
      }
      br.close();

      process.waitFor();
      process.destroy();

    } catch (Exception ex) {
    }
    return o;
  }
}


